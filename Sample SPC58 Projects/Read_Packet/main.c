/****************************************************************************
*
* Copyright © 2015-2021 STMicroelectronics - All Rights Reserved
*
* This software is licensed under SLA0098 terms that can be found in the
* DM00779817_1_0.pdf file in the licenses directory of this software product.
* 
* THIS SOFTWARE IS DISTRIBUTED "AS IS," AND ALL WARRANTIES ARE DISCLAIMED, 
* INCLUDING MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*
*****************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include <stdio.h>
#include <stdlib.h>
#include "components.h"
#include "PID.h"
#include "saradc_lld_cfg.h"
#include "pwm_lld_cfg.h"
#include "serial_lld_cfg.h"
/*
 * Application entry point.
 */

#define ADC_CHAN 63U
#define TICK_PERIOD 100 //10 kHz
#define DELAY 0
#define TRIGGER 0
#define CH0 0
#define CH1 1

static uint16_t value;
static uint8_t isDataReady;

void steering_column_adc_isr(SARADCDriver *saradcp){
	value = saradc_lld_readchannel(saradcp, ADC_CHAN);
	isDataReady = TRUE;
}

int main(void) {
	int8_t control;
	value = 0;
	isDataReady = FALSE;


  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/
  componentsInit();

  /* Uncomment the below routine to Enable Interrupts. */
  irqIsrEnable();

  // Enable PWM Group
  pwm_lld_init();
  pwm_lld_start(&PWMD9, &pwm_config_h_bridge);

  // Enable SARADC
  saradc_lld_start(&SARADC12DSV, &saradc_config_myConf);
  saradc_lld_start_conversion(&SARADC12DSV);

  while(!isDataReady);
  isDataReady = FALSE;
  PID_Init(value);

  PID_setPosition(1000);

  /* Application main loop.*/
  for ( ; ; ) {
	  while(osalThreadGetMilliseconds() % TS);
	  while(!isDataReady);

	  control = PID_update(value);

	  printf("ADC: %d, u0: %f\r\n", value, control);

	  if(control > 0){
		  pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, 0, DELAY, TRIGGER);
		  pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, abs(control), DELAY, TRIGGER);
	  }
	  else{
		  pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, 0, DELAY, TRIGGER);
		  pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, abs(control), DELAY, TRIGGER);
	  }


	  isDataReady = FALSE;
	  a1 = a0;
	  osalThreadDelayMilliseconds(1);

  }
}
