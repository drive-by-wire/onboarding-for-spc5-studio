/****************************************************************************
*
* Copyright © 2015-2021 STMicroelectronics - All Rights Reserved
*
* This software is licensed under SLA0098 terms that can be found in the
* DM00779817_1_0.pdf file in the licenses directory of this software product.
* 
* THIS SOFTWARE IS DISTRIBUTED "AS IS," AND ALL WARRANTIES ARE DISCLAIMED, 
* INCLUDING MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*
*****************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include "components.h"
#include "pwm_lld_cfg.h"

#define TICK_PERIOD 50 //10 kHz
#define DELAY 0
#define TRIGGER 0
#define CH0 0
#define CH1 1
#define CH2 2
#define CH3 3

/*
 * Application entry point.
 */
int main(void) {
	uint16_t offTicks = TICK_PERIOD >> 1;
  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/
  componentsInit();

  pwm_lld_init();
  pwm_lld_start(&PWMD9, &pwm_config_h_bridge);
  pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, offTicks, DELAY, TRIGGER);
  pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, offTicks, DELAY, TRIGGER);
  pwm_lld_enable_channel(&PWMD9, CH2, TICK_PERIOD, offTicks, DELAY, TRIGGER);
  pwm_lld_enable_channel(&PWMD9, CH3, TICK_PERIOD, offTicks, DELAY, TRIGGER);
  /* Uncomment the below routine to Enable Interrupts. */
  /* irqIsrEnable(); */
  
  /* Application main loop.*/
  for ( ; ; ) {

  }
}
