1. Enable PWMs in the "Low Level Drivers Components RLA"
2. Under Low Level Drivers->eMIOS Settings, set "eMIOS0 Group 1" to "OPWMB (PWM)"
3. Create a PWM Configuration
4. Select channel group "eMIOS0 Group1-3"
5. Set the prescaler and period to the desired frequency (I dont think the period does anything though).
6. Set the Channels needed to "ACTIVE LOW"
7. Generate and include "pwm_lld_cfg.h" in main
8. Init and start the group, then control each channel individually. 
