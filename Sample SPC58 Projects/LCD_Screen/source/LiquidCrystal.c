/*
 * LiquidCrystal.c
 *
 *  Created on: Apr 14, 2022
 *      Author: artur
 */

#include "LiquidCrystal.h"
#include "components.h"

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that it's in that state when a sketch starts (and the
// LiquidCrystal constructor is called).

#define LOW 0
#define HIGH 1

typedef struct {
//	uint8_t rs_pin; // LOW: command.  HIGH: character.
//	uint8_t rw_pin; // LOW: write to LCD.  HIGH: read from LCD. Tied to ground
//	uint8_t enable_pin; // activated by a HIGH pulse.
//	uint8_t data_pins[4];

	uint8_t displayfunction;
	uint8_t displaycontrol;
	uint8_t displaymode;

	uint8_t initialized;

	uint8_t numlines;
	uint8_t row_offsets[4];
} LiquidCrystal;

static LiquidCrystal lcd;

void init(void){

    lcd.displayfunction = LCD_4BITMODE | LCD_2LINE | LCD_5x8DOTS;

    //begin(16, 2);
}

void begin(uint8_t cols, uint8_t lines) {
	if(lines > 1)
		lcd.displayfunction |= LCD_2LINE;
	lcd.numlines = lines;

	setRowOffsets(0x00, 0x40, 0x00 + cols, 0x40 + cols);

	// for some 1 line displays you can select a 10 pixel high font

//	pinMode(lcd.rs_pin, OUTPUT); // Should already be set
//	pinMode(lcd.enable_pin, OUTPUT); // Should already be set

	// Do these once, instead of every time a character is drawn for speed reasons.
//	for (uint8_t i=0; i<((lcd.displayfunction & LCD_8BITMODE) ? 8 : 4); i++) // Should already be set
//	{
//	pinMode(_data_pins[i], OUTPUT);
//	}

	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40 ms after power rises above 2.7 V
	// before sending commands. Arduino can turn on way before 4.5 V so we'll wait 50
	uint32_t time = osalThreadGetMilliseconds();
	while(osalThreadGetMilliseconds() < time + 50);
//	osalThreadDelayMilliseconds(50);s
	// Now we pull both RS and R/W low to begin commands
	pal_lld_writepad(PORT_G, LCD_RS, LOW); //digitalWrite(_rs_pin, LOW);
	pal_lld_writepad(PORT_G, LCD_EN, LOW); //digitalWrite(_enable_pin, LOW);

	pal_lld_writepad(PORT_D, LCD_D0, HIGH);
	pal_lld_writepad(PORT_D, LCD_D1, HIGH);
	pal_lld_writepad(PORT_C, LCD_D2, HIGH);
	pal_lld_writepad(PORT_D, LCD_D3, HIGH);

	//put the LCD into 4 bit or 8 bit mode
	// this is according to the Hitachi HD44780 datasheet
	// figure 24, pg 46
	// we start in 8bit mode, try to set 4 bit mode
	write4bits(0x03);
	osalThreadDelayMicroseconds(4500); // wait min 4.1ms

	// second try
	write4bits(0x03);
	osalThreadDelayMicroseconds(4500); // wait min 4.1ms

	// third go!
	write4bits(0x03);
	osalThreadDelayMicroseconds(150);

	// finally, set to 4-bit interface
	write4bits(0x02);

	// finally, set # lines, font size, etc.
	command(LCD_FUNCTIONSET | lcd.displayfunction);

	// turn the display on with no cursor or blinking default
	lcd.displaycontrol = LCD_DISPLAYON | LCD_CURSORON | LCD_BLINKON;
	display();

	// clear it off
	//clear();

	// Initialize to default text direction (for romance languages)
	lcd.displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	// set the entry mode
	command(LCD_ENTRYMODESET | lcd.displaymode);
}

void setRowOffsets(int row0, int row1, int row2, int row3)
{
  lcd.row_offsets[0] = row0;
  lcd.row_offsets[1] = row1;
  lcd.row_offsets[2] = row2;
  lcd.row_offsets[3] = row3;
}

/********** high level commands, for the user! */
void clear(void)
{
  command(LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
  osalThreadDelayMicroseconds(2000);  // this command takes a long time!
}

void home(void)
{
  command(LCD_RETURNHOME);  // set cursor position to zero
  osalThreadDelayMicroseconds(2000);  // this command takes a long time!
}

void print(const char *str)
{
  while (*str)
    writeLC(*str++);
}

void setCursor(uint8_t col, uint8_t row)
{
  const size_t max_lines = sizeof(lcd.row_offsets) / sizeof(*lcd.row_offsets);
  if ( row >= max_lines ) {
    row = max_lines - 1;    // we count rows starting w/ 0
  }
  if ( row >= lcd.numlines ) {
    row = lcd.numlines - 1;    // we count rows starting w/ 0
  }

  command(LCD_SETDDRAMADDR | (col + lcd.row_offsets[row]));
}

// Turn the display on/off (quickly)
void noDisplay(void) {
  lcd.displaycontrol &= ~LCD_DISPLAYON;
  command(LCD_DISPLAYCONTROL | lcd.displaycontrol);
}
void display(void) {
  lcd.displaycontrol |= LCD_DISPLAYON;
  command(LCD_DISPLAYCONTROL | lcd.displaycontrol);
}

// Turns the underline cursor on/off
void noCursor(void) {
  lcd.displaycontrol &= ~LCD_CURSORON;
  command(LCD_DISPLAYCONTROL | lcd.displaycontrol);
}
void cursor(void) {
  lcd.displaycontrol |= LCD_CURSORON;
  command(LCD_DISPLAYCONTROL | lcd.displaycontrol);
}

// Turn on and off the blinking cursor
void noBlink(void) {
  lcd.displaycontrol &= ~LCD_BLINKON;
  command(LCD_DISPLAYCONTROL | lcd.displaycontrol);
}
void blink(void) {
  lcd.displaycontrol |= LCD_BLINKON;
  command(LCD_DISPLAYCONTROL | lcd.displaycontrol);
}

// These commands scroll the display without changing the RAM
void scrollDisplayLeft(void) {
  command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void scrollDisplayRight(void) {
  command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void leftToRight(void) {
  lcd.displaymode |= LCD_ENTRYLEFT;
  command(LCD_ENTRYMODESET | lcd.displaymode);
}

// This is for text that flows Right to Left
void rightToLeft(void) {
  lcd.displaymode &= ~LCD_ENTRYLEFT;
  command(LCD_ENTRYMODESET | lcd.displaymode);
}

// This will 'right justify' text from the cursor
void autoscroll(void) {
  lcd.displaymode |= LCD_ENTRYSHIFTINCREMENT;
  command(LCD_ENTRYMODESET | lcd.displaymode);
}

// This will 'left justify' text from the cursor
void noAutoscroll(void) {
  lcd.displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
  command(LCD_ENTRYMODESET | lcd.displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void createChar(uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
  command(LCD_SETCGRAMADDR | (location << 3));
  for (int i=0; i<8; i++) {
    writeLC(charmap[i]);
  }
}

/*********** mid level commands, for sending data/cmds */

inline void command(uint8_t value) {
  send(value, LOW);
}

uint8_t writeLC(uint8_t value) {
  send(value, HIGH);
  return 1; // assume success
}

/************ low level data pushing commands **********/

// write either command or data, with automatic 4/8-bit selection
void send(uint8_t value, uint8_t mode) {
  pal_lld_writepad(PORT_G, LCD_RS, mode); //digitalWrite(_rs_pin, mode);

  // if there is a RW pin indicated, set it low to Write
//  if (_rw_pin != 255) {
//    digitalWrite(_rw_pin, LOW);
//  }

  if (lcd.displayfunction & LCD_8BITMODE) {
    write8bits(value);
  } else {
    write4bits(value>>4);
    write4bits(value);
  }
}

void pulseEnable(void) {
  pal_lld_writepad(PORT_G, LCD_EN, LOW); //digitalWrite(_enable_pin, LOW);
  osalThreadDelayMicroseconds(1);
  pal_lld_writepad(PORT_G, LCD_EN, HIGH);//digitalWrite(_enable_pin, HIGH);
  osalThreadDelayMicroseconds(1);    // enable pulse must be >450 ns
  pal_lld_writepad(PORT_G, LCD_EN, LOW); //digitalWrite(_enable_pin, LOW);
  osalThreadDelayMicroseconds(100);   // commands need >37 us to settle
}

void write4bits(uint8_t value) {
//  for (int i = 0; i < 4; i++) {
//    //digitalWrite(_data_pins[i], (value >> i) & 0x01);
//	  pal_lld_writepad(PORT_F, lcd.data_pins[i], (value >> i) & 0x01);
//  }
	pal_lld_writepad(PORT_D, LCD_D0, value & 0x01);
	pal_lld_writepad(PORT_D, LCD_D1, (value >> 1) & 0x01);
	pal_lld_writepad(PORT_C, LCD_D2, (value >> 2) & 0x01);
	pal_lld_writepad(PORT_D, LCD_D3, (value >> 3) & 0x01);

  pulseEnable();
}

void write8bits(uint8_t value) {
//  for (int i = 0; i < 8; i++) {
//	  pal_lld_writepad(PORT_F, lcd.data_pins[i], (value >> i) & 0x01); //digitalWrite(_data_pins[i], (value >> i) & 0x01);
//  }

	pal_lld_writepad(PORT_D, LCD_D0, value & 0x01);
		pal_lld_writepad(PORT_D, LCD_D1, (value >> 1) & 0x01);
		pal_lld_writepad(PORT_C, LCD_D2, (value >> 2) & 0x01);
		pal_lld_writepad(PORT_D, LCD_D3, (value >> 3) & 0x01);
  pulseEnable();
}
