/*
 * speedomter.c
 *
 *  Created on: Apr 18, 2022
 *      Author: artur
 */

#include "speedometer.h"
#include "components.h"


typedef enum{
	STATE0,
	STATE1,
	STATE2,
	STATE3,
	STATE4,
	STATE5,
	STATE6
}State;

typedef struct{
	uint32_t prevTime; // Previous rotation time in microseconds
	uint32_t period; // Current period
	uint8_t rot; // Rotation direction
	uint8_t tick;
	State state;
} Speedometer;

void speedometer_updatePeriod(void);

static Speedometer speedometer;

uint8_t speedometer_init(void){
	speedometer.prevTime = 0;
	speedometer.period = 0xFFFFFFFF; // The Period in Microseconds
	speedometer.rot = CW;
	speedometer.tick = FALSE;
	speedometer.state = STATE0;
	return 1;
}

uint8_t speedometer_update(uint8_t chanA, uint8_t chanB){
	if(speedometer.state == STATE0){
		if(!chanA && chanB){
			speedometer.state = STATE1;
			speedometer.rot = CW;
		}
		else if(chanA && !chanB){
			speedometer.state = STATE4;
			speedometer.rot = CCW;
		}
		else if(!(chanA || chanB || (speedometer.rot == CCW)))
			speedometer.state = STATE2;
		else if(!(chanA || chanB) && speedometer.rot == CCW)
			speedometer.state = STATE5;
	}
	else if(speedometer.state == STATE1){
		if(chanA && chanB)
			speedometer.state = STATE0;
		else if(chanA && !chanB)
			speedometer.state = STATE2;
		else if(chanA && !chanB && speedometer.rot == CCW)
			speedometer.state = STATE4;
		else if(chanA && !(chanB || (speedometer.rot == CCW)))
			speedometer.state = STATE3;
	}
	else if(speedometer.state == STATE2){
		if(!chanA && chanB)
			speedometer.state = STATE1;
		else if(chanA && !chanB)
			speedometer.state = STATE3;
		else if(chanA && chanB){
			speedometer.state = STATE0;
			if(speedometer.rot == CW){
				speedometer_updatePeriod();
				speedometer.tick = TRUE;
				return TRUE;
			}
		}
	}
	else if(speedometer.state == STATE3){
		if(chanA && chanB){
			speedometer.state = STATE0;
			speedometer_updatePeriod();
			speedometer.tick = TRUE;
			return TRUE;
		}
		else if(!chanA && chanB){
			speedometer.state = STATE1;
			if(speedometer.rot == CW){
				speedometer_updatePeriod();
				speedometer.tick = TRUE;
				return TRUE;
			}
		}
		else if(!(chanA || chanB))
			speedometer.state = STATE2;
	}
	else if(speedometer.state == STATE4){
		if(!(chanA || chanB))
			speedometer.state = STATE5;
		else if(chanA && chanB)
			speedometer.state = STATE0;
		else if(!chanA && chanB && speedometer.rot == CCW)
			speedometer.state = STATE6;
		else if (!chanA && chanB && speedometer.rot == CW)
			speedometer.state = STATE1;
	}
	else if(speedometer.state == STATE5){
		if(!chanA && chanB)
			speedometer.state = STATE6;
		else if(chanA && !chanB)
			speedometer.state = STATE4;
		else if(chanA && chanB){
			speedometer.state = STATE0;
			if(speedometer.rot == CCW){
				speedometer_updatePeriod();
				speedometer.tick = TRUE;
				return TRUE;
			}
		}
	}
	else if(speedometer.state == STATE6){
		if(chanA && chanB){
			speedometer.state = STATE0;
			speedometer_updatePeriod();
			speedometer.tick = TRUE;
			return TRUE;
		}
		else if(!(chanA || chanB))
			speedometer.state = STATE5;
		else if(chanA && !chanB){
			speedometer.state = STATE4;
			if (speedometer.rot == CCW){
				speedometer_updatePeriod();
				speedometer.tick = TRUE;
				return TRUE;
			}
		}
	}
	return FALSE;
}

uint8_t speedometer_hasRotOccured(void){
	if(speedometer.tick){
		speedometer.tick = FALSE;
		return TRUE;
	}
	return FALSE;
}

uint32_t speedometer_getPeriodMicroseconds(void){
	return speedometer.period;
}

float speedometer_getSpeed(void){
	return 72500 / (float)speedometer.period;
}

uint8_t speedometer_reset(void){
	speedometer_init();
	return 1;
}

void speedometer_updatePeriod(void){
	uint32_t curTime = osalThreadGetMicroseconds();
	speedometer.period = curTime - speedometer.prevTime;
	speedometer.prevTime = curTime;
}

