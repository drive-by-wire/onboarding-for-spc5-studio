/*
 * speedometer.h
 *
 *  Created on: Apr 18, 2022
 *      Author: artur
 */

#ifndef INCLUDE_SPEEDOMETER_H_
#define INCLUDE_SPEEDOMETER_H_

#include "components.h"

#define CW 0
#define CCW 1

uint8_t speedometer_init(void);

uint8_t speedometer_update(uint8_t chanA, uint8_t chanB);

uint8_t speedometer_hasRotOccured(void);

uint32_t speedometer_getPeriodMicroseconds(void);

float speedometer_getSpeed(void);

uint8_t speedometer_reset(void);

#endif /* INCLUDE_SPEEDOMETER_H_ */
