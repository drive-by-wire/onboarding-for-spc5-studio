/****************************************************************************
*
* Copyright © 2015-2021 STMicroelectronics - All Rights Reserved
*
* This software is licensed under SLA0098 terms that can be found in the
* DM00779817_1_0.pdf file in the licenses directory of this software product.
* 
* THIS SOFTWARE IS DISTRIBUTED "AS IS," AND ALL WARRANTIES ARE DISCLAIMED, 
* INCLUDING MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*
*****************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/

#include <stdio.h>
#include "components.h"
#include "icu_lld_cfg.h"
#include "speedometer.h"


//uint8_t update_speedometer(void);

void period_isr(ICUDriver *icup){
	(void)icup;
//	uint32_t curTime;
	uint8_t chanA = pal_lld_readpad(PORT_A, PIN_UC1_0);
	uint8_t chanB = pal_lld_readpad(PORT_F, PIN_UC2_0);
	speedometer_update(chanA, chanB);
//	if(flag){
////		speedometer.period = curTime - speedometer.prevTime;
////		speedometer.prevTime = curTime;
//		printf("Period: %ld\r\n", speedometer_getPeriodMicroseconds());
//	}
}

void width_isr(ICUDriver *icup){
	(void)icup;
//		uint32_t curTime;
		uint8_t chanA = pal_lld_readpad(PORT_A, PIN_UC1_0);
		uint8_t chanB = pal_lld_readpad(PORT_F, PIN_UC2_0);
		speedometer_update(chanA, chanB);
//		if(flag){
////			curTime = osalThreadGetMicroseconds();
//	//		speedometer.period = curTime - speedometer.prevTime;
//	//		speedometer.prevTime = curTime;
//			printf("Period: %ld\r\n", speedometer_getPeriodMicroseconds());
//		}
}

/*
 * Application entry point.
 */
int main(void) {
  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/
  componentsInit();

  /* Uncomment the below routine to Enable Interrupts. */

  speedometer_init();

  irqIsrEnable();
  
  icu_lld_start(&ICUD2, &icu_config_icu_cfg);
  icu_lld_start(&ICUD3, &icu_config_icu_cfg);
  icu_lld_enable(&ICUD2);
  icu_lld_enable(&ICUD3);

  printf("Hello World\r\n");
//  osalThreadGetMicroseconds(); // Returns the thread runtime in microseconds.
  	  	  	  	  	  	  	   // I do not know if this works with the thread delay call

  /* Application main loop.*/
  for ( ; ; ) {
	  if(speedometer_hasRotOccured())
		  printf("Period: %ld\r\n", speedometer_getPeriodMicroseconds());
  }
}

//uint8_t update_speedometer(void){
//	uint8_t chanA = pal_lld_readpad(PORT_A, PIN_UC1_0);
//	uint8_t chanB = pal_lld_readpad(PORT_F, PIN_UC2_0);
//	if(speedometer.state == STATE0){
//		if(!chanA && chanB){
//			speedometer.state = STATE1;
//			speedometer.rot = CW;
//		}
//		else if(chanA && !chanB){
//			speedometer.state = STATE4;
//			speedometer.rot = CCW;
//		}
//		else if(!(chanA || chanB || (speedometer.rot == CCW)))
//			speedometer.state = STATE2;
//		else if(!(chanA || chanB) && speedometer.rot == CCW)
//			speedometer.state = STATE5;
//	}
//	else if(speedometer.state == STATE1){
//		if(chanA && chanB)
//			speedometer.state = STATE0;
//		else if(chanA && !chanB)
//			speedometer.state = STATE2;
//		else if(chanA && !chanB && speedometer.rot == CCW)
//			speedometer.state = STATE4;
//		else if(chanA && !(chanB || (speedometer.rot == CCW)))
//			speedometer.state = STATE3;
//	}
//	else if(speedometer.state == STATE2){
//		if(!chanA && chanB)
//			speedometer.state = STATE1;
//		else if(chanA && !chanB)
//			speedometer.state = STATE3;
//		else if(chanA && chanB){
//			speedometer.state = STATE0;
//			if(speedometer.rot == CW)
//				return TRUE;
//		}
//	}
//	else if(speedometer.state == STATE3){
//		if(chanA && chanB){
//			speedometer.state = STATE0;
//			return TRUE;
//		}
//		else if(!chanA && chanB){
//			speedometer.state = STATE1;
//			if(speedometer.rot == CW)
//				return TRUE;
//		}
//		else if(!(chanA || chanB))
//			speedometer.state = STATE2;
//	}
//	else if(speedometer.state == STATE4){
//		if(!(chanA || chanB))
//			speedometer.state = STATE5;
//		else if(chanA && chanB)
//			speedometer.state = STATE0;
//		else if(!chanA && chanB && speedometer.rot == CCW)
//			speedometer.state = STATE6;
//		else if (!chanA && chanB && speedometer.rot == CW)
//			speedometer.state = STATE1;
//	}
//	else if(speedometer.state == STATE5){
//		if(!chanA && chanB)
//			speedometer.state = STATE6;
//		else if(chanA && !chanB)
//			speedometer.state = STATE4;
//		else if(chanA && chanB){
//			speedometer.state = STATE0;
//			if(speedometer.rot == CCW)
//				return TRUE;
//		}
//	}
//	else if(speedometer.state == STATE6){
//		if(chanA && chanB){
//			speedometer.state = STATE0;
//			return TRUE;
//		}
//		else if(!(chanA || chanB))
//			speedometer.state = STATE5;
//		else if(chanA && !chanB){
//			speedometer.state = STATE4;
//			if (speedometer.rot == CCW)
//				return TRUE;
//		}
//	}
//	return FALSE;
//}



