----------------------------------------------------------

  Connection Failed Report from
  Basic UDE Target Interface, Version: 1.18.9
  created: 04/11/22, 12:28:11

----------------------------------------------------------

Windows version:
  Win8 ()
  Admin: yes

UDE version:
  Release:  5.02.04
  Build:    8310
  Path:     C:\Program Files (x86)\pls\UDE Starterkit 5.2

Target configuration file:
  C:\Users\artur\Desktop\central_controller\SPC58_Workspace\ADC_Test\UDE\stm_spc58ec80_core2_core0_debug_jtag.cfg

Error messages:
  PpcJtagTargIntf: Can't connect target !
  PpcJtagTargIntf: No JTAG client found !
Please check:
- target power supply
- JTAG cable connection
- target configuration

Settings:
  PortType:  Default
  CommDevSel:  
  JtagViaPod:  n
  TargetPort:  Default
  JtagTapNumber:  0
  JtagNumOfTaps:  1
  JtagNumIrBefore:  0
  JtagNumIrAfter:  0
  UseExtendedCanId:  n
  JtagOverCanIdA:  0x00000001
  JtagOverCanIdB:  0x00000002
  JtagOverCanIdC:  0x00000003
  JtagOverCanIdD:  0x00000004
  JtagOverCanIdE:  0x00000005
  JtagmTckSel:  3
  JtagmInterFrameTimer:  0
  MaxJtagClk:  5000
  AdaptiveJtagPhaseShift:  y
  JtagMuxPort:  0
  JtagMuxWaitTime:  0
  JtagIoType:  Jtag
  EtksArbiterMode:  None
  EtksMicroSecondTimeout:  100
  CheckJtagId:  y
  ConnOption:  Default
  UseExtReset:  y
  SetDebugEnableAb1DisablePin:  n
  OpenDrainReset:  n
  ResetWaitTime:  50
  HaltAfterReset:  y
  ChangeJtagClk:  4294967295
  ExecInitCmds:  y
  InvalidateCache:  y
  ChangeMsr:  n
  ChangeMsrValue:  0x00000000
  ResetPulseLen:  5
  InitScript Script:
    // disable watchdog
    SET 0xF4058010 0x0000C520
    SET 0xF4058010 0x0000D928
    SET 0xF4058000 0xFF00000A
    
    // cache invalidate
    SETSPR 0x3F2 0x00000003 0x00000003
    SETSPR 0x3F3 0x00000003 0x00000003
    SETSPR 0x3F2 0x00000000 0x00000003
    SETSPR 0x3F3 0x00000000 0x00000003
    
    // setup IVOPR
    // points to internal flash at 0x01000000
    SETSPR 0x3F 0x01000000 0xFFFFFFFF
    
    // setup SSCM error cfg for debug
    //SET16 SSCM_ERROR 0x3
    
    // reset CGM_AC0_SC to reset value because ESR0/PORST do not do so
    // CGM_AC0_SC must provide a valid clock in order to allow GTM debugging
    SET CGM_AC0_SC 0x00000000
    //SET CGM_AC0_SC 0x00000000
    
    // reset clock selector because it is not reset by hardware
    SET CGM_AC12_SC 0x00000000
    
    // disable reset escalation
    SET8 RGM_FRET 0x00
    SET8 RGM_DRET 0x00
    
  ExecOnConnectCmds:  n
  OnConnectScript Script:
    
  SimioAddr:  g_JtagSimioAccess
  FreezeTimers:  y
  AllowMmuSetup:  n
  ExecOnStartCmds:  n
  OnStartScript Script:
    
  ExecOnHaltCmds:  n
  ExecOnHaltCmdsWhileHalted:  n
  OnHaltScript Script:
    
  EnableProgramTimeMeasurement:  y
  TimerForPTM:  Default
  DefUserStreamChannel:  0
  DontUseCachedRegisters:  n
  AllowBreakOnUpdateBreakpoints:  n
  ClearDebugStatusOnHalt:  y
  UseRestartWhileRunningHandling:  n
  DoNotEnableSwBrk:  n
  TargetAppHandshakeMode:  None
  TargetAppHandshakeTimeout:  100
  TargetAppHandshakeParameter0:  0x00000000
  TargetAppHandshakeParameter1:  0x00000000
  TargetAppHandshakeParameter2:  0x00000000
  TargetAppHandshakeParameter3:  0x00000000
  UseNexus:  y
  DoSramInit:  y
  ForceCacheFlush:  n
  IgnoreLockedLines:  n
  HandleWdtBug:  n
  ForceEndOfReset:  n
  UseHwResetMode:  n
  HwResetMode:  Simulate
  WaitForEndOfBootCode:  n
  HandleNexusAccessBug:  y
  UseMasterNexusIfResetState:  y
  UseLocalAddressTranslation:  y
  Use64BitNexus:  n
  InitSramOnlyWhenNotInitialized:  n
  AllowHarrForUpdateDebugRegs:  n
  HaltOnDnh:  y
  AlwaysHaltOnDni:  y
  EnableLowPowerDebugHandshake:  n
  EnableLockstepDebug:  y
  AddBranchBeforeGo:  n
  InvalidTlbOnReset:  n
  DoNotEnableTrapSwBrp:  n
  AllowResetOnCheck:  n
  BootPasswd0:  0xFEEDFACE
  BootPasswd1:  0xCAFEBEEF
  BootPasswd2:  0xFEEDFACE
  BootPasswd3:  0xCAFEBEEF
  BootPasswd4:  0xFEEDFACE
  BootPasswd5:  0xCAFEBEEF
  BootPasswd6:  0xFEEDFACE
  BootPasswd7:  0xCAFEBEEF
  PasswordFile:  
  UsePasswordForUnlockDevice:  y
  DisableE2EECC:  n
  UseCore0ForNexusMemoryAccessWhileRunning:  n
  ForceDniForDebugger:  n
  HandleOvRamInitViaNarWorkaround:  n
  ApplySPC58NE84XoscWorkaround:  y
  ApplyEigerEdJtagWorkaround:  n
  IsUsedByTester:  n
  Mpc57xxClearPeripheralDebugAtNextCheckUserAppWhenRunning:  n
  SlaveHasHalted:  n

JTAG target infos:
  JTAG-ID:           0x00000000
  UsedJtagClk:       3000 kHz
  ExtVoltage:        0.0 V
  IntVoltageUsed:    n

Target infos:
  CoreName:  Core2
  FullCoreName:  Controller0.Core2
  ExtClock:  40000000
  IntClock:  100000000
  SysClock:  0
  StmClock:  0
  AccessToken:  0x39BD
  HasNexus:  n
  BigEndian:  n
  CanSimio:  n
  CanPhysicalAccess:  n
  HasSpe:  n
  NumOfSimioChannels:  0
  JtagId:  0x00000000
  IsEarlyStep:  n
  IsMaster:  y
  MasterCoreName:  
  IsMasterEnabled:  y
  IsSlave:  n
  BuddyDeviceDetected:  n
  EtkConnected:  n
  Data TLB size on target:  0x00000000
  Instruction TLB size on target:  0x00000000
  Shared TLB size on target:  0x00000000
  Number of data TLB entries:  0x00000000
  Number of instruction TLB entries:  0x00000000
  Number of shared TLB entries:  0x00000000
  Extended E200 MMU:  n
  E200 MPU:  n
  Data cache size:  0x00000000
  Data cache ways:  0x00000000
  Data cache sets:  0x00000000
  Data cache entry size:  0x00000000
  Instruction cache size:  0x00000000
  Instruction cache ways:  0x00000000
  Instruction cache sets:  0x00000000
  Instruction cache entry size:  0x00000000
  Unified Cache:  n
  MCM base address:  0xF4028000
  SIU base address:  0xF7FC0000
  Nexus On Slave:  n
  Core Number:  2
  Has Wdt bug:  n
  Length of IR register:  0x00000006
  Has Data Value comparators:  y
  Reset Mode:  0x00000008
  STM timer base address:  0xF4070000
  MC_ME base address:  0xF7FB8000
  Core in Lockstep mode:  n
  Core in DPM mode:  n
  Core is HSM:  n
  Core is Master of HSM:  n
  Name of other Master Core:  
  HsmBootEnabled:  n
  Target has Nexus access bug:  n
  Target has unlock JTAG capability:  y
  Unlock JTAG password len:  0x00000100
  Has JTAG unlock enable bit:  n
  ExecuteOpcodeAddr:  0x01000000
  IMEMBaseAddr:  0xFFFFFFFF
  IMEMSize:  0x00000000
  DMEMBaseAddr:  0x52800000
  DMEMSize:  0x00010000
  BootCodeStartAddr:  0x00404100
  HasCJtag:  y
  HasLfast:  n
  HasJtagOverCan:  y
  HasNpcLowPowerHandshake:  n
  HasLockstepDebug:  n
  TargetIsForSpc5UdeStk:  y
  PllCalc:  UDE.SPC58ECPllCalc
  JtagIdWhiteList:  0x00142041,0x0FFFFFFF
  JtagIdBlackList:  
  DciPinControl:  
  DciControl:  
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000001
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000000
  JtagChainType:  UNKNOWN
  JtagChainNumber:  0x00000000
  PowerPc system type:  MCKINLEY
  PowerPc synchonized GO type:  MPC5XXXPAR
  InactiveAfterReset:  n
  NumOfActiveCores:  1

Communication device:
  Type/Firmware:  FtdiCommDev V2.1.1
  Serial Number:  39511

Communication protocol handler:
  LastCmd:      0x0410
  LastResult:   0xC000
  ExpBytes:     580 (0x0244)
  RetBytes:     580 (0x0244)
  LastTimeout:  120010

Protocol diagnostic output:
  LastJtagApiAddr:   0x00000000
  LastJtagApiSpr:    0x00000000
  LastJtagApiDcr:    0x00000000
  LastJtagApiError:  0x00000000
  LastJtagApiStatus: 0x00000000
  JtagApiErrorLine:  1721
  JtagApiAddInfo0:   0x00000000
  JtagApiAddInfo1:   0x00000000
  ProtErrorLine:     5218
  LowLevelDiag0:     DEADBEEF (3735928559)
  LowLevelDiag1:     DEADBEEF (3735928559)
  LowLevelDiag2:     DEADBEEF (3735928559)
  LowLevelDiag3:     DEADBEEF (3735928559)


----------------------------------------------------------

  Connection Failed Report from
  Basic UDE Target Interface, Version: 1.18.9
  created: 04/11/22, 14:02:38

----------------------------------------------------------

Windows version:
  Win8 ()
  Admin: yes

UDE version:
  Release:  5.02.04
  Build:    8310
  Path:     C:\Program Files (x86)\pls\UDE Starterkit 5.2

Target configuration file:
  C:\Users\artur\Desktop\central_controller\SPC58_Workspace\ADC_Test\UDE\stm_spc58ec80_core2_core0_debug_jtag.cfg

Settings:
  PortType:  Default
  CommDevSel:  
  JtagViaPod:  n
  TargetPort:  Default
  JtagTapNumber:  0
  JtagNumOfTaps:  1
  JtagNumIrBefore:  0
  JtagNumIrAfter:  0
  UseExtendedCanId:  n
  JtagOverCanIdA:  0x00000001
  JtagOverCanIdB:  0x00000002
  JtagOverCanIdC:  0x00000003
  JtagOverCanIdD:  0x00000004
  JtagOverCanIdE:  0x00000005
  JtagmTckSel:  3
  JtagmInterFrameTimer:  0
  MaxJtagClk:  5000
  AdaptiveJtagPhaseShift:  y
  JtagMuxPort:  0
  JtagMuxWaitTime:  0
  JtagIoType:  Jtag
  EtksArbiterMode:  None
  EtksMicroSecondTimeout:  100
  CheckJtagId:  y
  ConnOption:  Default
  UseExtReset:  y
  SetDebugEnableAb1DisablePin:  n
  OpenDrainReset:  n
  ResetWaitTime:  50
  HaltAfterReset:  y
  ChangeJtagClk:  4294967295
  ExecInitCmds:  y
  InvalidateCache:  y
  ChangeMsr:  n
  ChangeMsrValue:  0x00000000
  ResetPulseLen:  5
  InitScript Script:
    // disable watchdog
    SET 0xF4058010 0x0000C520
    SET 0xF4058010 0x0000D928
    SET 0xF4058000 0xFF00000A
    
    // cache invalidate
    SETSPR 0x3F2 0x00000003 0x00000003
    SETSPR 0x3F3 0x00000003 0x00000003
    SETSPR 0x3F2 0x00000000 0x00000003
    SETSPR 0x3F3 0x00000000 0x00000003
    
    // setup IVOPR
    // points to internal flash at 0x01000000
    SETSPR 0x3F 0x01000000 0xFFFFFFFF
    
    // setup SSCM error cfg for debug
    //SET16 SSCM_ERROR 0x3
    
    // reset CGM_AC0_SC to reset value because ESR0/PORST do not do so
    // CGM_AC0_SC must provide a valid clock in order to allow GTM debugging
    SET CGM_AC0_SC 0x00000000
    //SET CGM_AC0_SC 0x00000000
    
    // reset clock selector because it is not reset by hardware
    SET CGM_AC12_SC 0x00000000
    
    // disable reset escalation
    SET8 RGM_FRET 0x00
    SET8 RGM_DRET 0x00
    
  ExecOnConnectCmds:  n
  OnConnectScript Script:
    
  SimioAddr:  g_JtagSimioAccess
  FreezeTimers:  y
  AllowMmuSetup:  n
  ExecOnStartCmds:  n
  OnStartScript Script:
    
  ExecOnHaltCmds:  n
  ExecOnHaltCmdsWhileHalted:  n
  OnHaltScript Script:
    
  EnableProgramTimeMeasurement:  y
  TimerForPTM:  Default
  DefUserStreamChannel:  0
  DontUseCachedRegisters:  n
  AllowBreakOnUpdateBreakpoints:  n
  ClearDebugStatusOnHalt:  y
  UseRestartWhileRunningHandling:  n
  DoNotEnableSwBrk:  n
  TargetAppHandshakeMode:  None
  TargetAppHandshakeTimeout:  100
  TargetAppHandshakeParameter0:  0x00000000
  TargetAppHandshakeParameter1:  0x00000000
  TargetAppHandshakeParameter2:  0x00000000
  TargetAppHandshakeParameter3:  0x00000000
  UseNexus:  y
  DoSramInit:  y
  ForceCacheFlush:  n
  IgnoreLockedLines:  n
  HandleWdtBug:  n
  ForceEndOfReset:  n
  UseHwResetMode:  n
  HwResetMode:  Simulate
  WaitForEndOfBootCode:  n
  HandleNexusAccessBug:  y
  UseMasterNexusIfResetState:  y
  UseLocalAddressTranslation:  y
  Use64BitNexus:  n
  InitSramOnlyWhenNotInitialized:  n
  AllowHarrForUpdateDebugRegs:  n
  HaltOnDnh:  y
  AlwaysHaltOnDni:  y
  EnableLowPowerDebugHandshake:  n
  EnableLockstepDebug:  y
  AddBranchBeforeGo:  n
  InvalidTlbOnReset:  n
  DoNotEnableTrapSwBrp:  n
  AllowResetOnCheck:  n
  BootPasswd0:  0xFEEDFACE
  BootPasswd1:  0xCAFEBEEF
  BootPasswd2:  0xFEEDFACE
  BootPasswd3:  0xCAFEBEEF
  BootPasswd4:  0xFEEDFACE
  BootPasswd5:  0xCAFEBEEF
  BootPasswd6:  0xFEEDFACE
  BootPasswd7:  0xCAFEBEEF
  PasswordFile:  
  UsePasswordForUnlockDevice:  y
  DisableE2EECC:  n
  UseCore0ForNexusMemoryAccessWhileRunning:  n
  ForceDniForDebugger:  n
  HandleOvRamInitViaNarWorkaround:  n
  ApplySPC58NE84XoscWorkaround:  y
  ApplyEigerEdJtagWorkaround:  n
  IsUsedByTester:  n
  Mpc57xxClearPeripheralDebugAtNextCheckUserAppWhenRunning:  n
  SlaveHasHalted:  n

JTAG target infos:
  JTAG-ID:           0x10142041
  UsedJtagClk:       3000 kHz
  ExtVoltage:        0.0 V
  IntVoltageUsed:    n

Target infos:
  CoreName:  Core2
  FullCoreName:  Controller0.Core2
  ExtClock:  40000000
  IntClock:  16000000
  SysClock:  4000000
  StmClock:  8000000
  AccessToken:  0x4C1C
  HasNexus:  y
  BigEndian:  y
  CanSimio:  y
  CanPhysicalAccess:  y
  HasSpe:  n
  NumOfSimioChannels:  1
  JtagId:  0x10142041
  IsEarlyStep:  n
  IsMaster:  y
  MasterCoreName:  
  IsMasterEnabled:  y
  IsSlave:  n
  BuddyDeviceDetected:  n
  EtkConnected:  n
  Data TLB size on target:  0x00000010
  Instruction TLB size on target:  0x00000010
  Shared TLB size on target:  0x00000010
  Number of data TLB entries:  0x0000000C
  Number of instruction TLB entries:  0x00000006
  Number of shared TLB entries:  0x00000006
  Extended E200 MMU:  n
  E200 MPU:  y
  Data cache size:  0x00001000
  Data cache ways:  0x00000002
  Data cache sets:  0x00000040
  Data cache entry size:  0x00000024
  Instruction cache size:  0x00002000
  Instruction cache ways:  0x00000002
  Instruction cache sets:  0x00000080
  Instruction cache entry size:  0x00000024
  Unified Cache:  n
  MCM base address:  0xF4028000
  SIU base address:  0xF7FC0000
  Nexus On Slave:  n
  Core Number:  2
  Has Wdt bug:  n
  Length of IR register:  0x00000006
  Has Data Value comparators:  y
  Reset Mode:  0x00000008
  STM timer base address:  0xF4070000
  MC_ME base address:  0xF7FB8000
  Core in Lockstep mode:  n
  Core in DPM mode:  n
  Core is HSM:  n
  Core is Master of HSM:  n
  Name of other Master Core:  
  HsmBootEnabled:  n
  Target has Nexus access bug:  n
  Target has unlock JTAG capability:  y
  Unlock JTAG password len:  0x00000100
  Has JTAG unlock enable bit:  n
  ExecuteOpcodeAddr:  0x01000000
  IMEMBaseAddr:  0xFFFFFFFF
  IMEMSize:  0x00000000
  DMEMBaseAddr:  0x52800000
  DMEMSize:  0x00010000
  BootCodeStartAddr:  0x00404100
  HasCJtag:  y
  HasLfast:  n
  HasJtagOverCan:  y
  HasNpcLowPowerHandshake:  n
  HasLockstepDebug:  n
  TargetIsForSpc5UdeStk:  y
  PllCalc:  UDE.SPC58ECPllCalc
  JtagIdWhiteList:  0x00142041,0x0FFFFFFF
  JtagIdBlackList:  
  DciPinControl:  
  DciControl:  
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000001
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000000
  JtagChainType:  UNKNOWN
  JtagChainNumber:  0x00000000
  PowerPc system type:  MCKINLEY
  PowerPc synchonized GO type:  MPC5XXXPAR
  InactiveAfterReset:  y
  NumOfActiveCores:  1

Communication device:
  Type/Firmware:  FtdiCommDev V2.1.1
  Serial Number:  39511

Communication protocol handler:
  LastCmd:      0x0412
  LastResult:   0xC01C
  ExpBytes:     560 (0x0230)
  RetBytes:     560 (0x0230)
  LastTimeout:  10000

Protocol diagnostic output:
  LastJtagApiAddr:   0x400A8400
  LastJtagApiSpr:    0x00000008
  LastJtagApiDcr:    0x00000000
  LastJtagApiError:  0x00000000
  LastJtagApiStatus: 0x00000201
  JtagApiErrorLine:  5663
  JtagApiAddInfo0:   0x00000000
  JtagApiAddInfo1:   0x00000000
  ProtErrorLine:     1471
  LowLevelDiag0:     00000000 (0)
  LowLevelDiag1:     00000000 (0)
  LowLevelDiag2:     00000000 (0)
  LowLevelDiag3:     00000000 (0)


----------------------------------------------------------

  Connection Failed Report from
  Basic UDE Target Interface, Version: 1.18.9
  created: 04/11/22, 14:03:06

----------------------------------------------------------

Windows version:
  Win8 ()
  Admin: yes

UDE version:
  Release:  5.02.04
  Build:    8310
  Path:     C:\Program Files (x86)\pls\UDE Starterkit 5.2

Target configuration file:
  C:\Users\artur\Desktop\central_controller\SPC58_Workspace\ADC_Test\UDE\stm_spc58ec80_core2_core0_debug_jtag.cfg

Error messages:
  PpcJtagTargIntf: Can't connect target !
  PpcJtagTargIntf: No JTAG client found !
Please check:
- target power supply
- JTAG cable connection
- target configuration

Settings:
  PortType:  Default
  CommDevSel:  
  JtagViaPod:  n
  TargetPort:  Default
  JtagTapNumber:  0
  JtagNumOfTaps:  1
  JtagNumIrBefore:  0
  JtagNumIrAfter:  0
  UseExtendedCanId:  n
  JtagOverCanIdA:  0x00000001
  JtagOverCanIdB:  0x00000002
  JtagOverCanIdC:  0x00000003
  JtagOverCanIdD:  0x00000004
  JtagOverCanIdE:  0x00000005
  JtagmTckSel:  3
  JtagmInterFrameTimer:  0
  MaxJtagClk:  5000
  AdaptiveJtagPhaseShift:  y
  JtagMuxPort:  0
  JtagMuxWaitTime:  0
  JtagIoType:  Jtag
  EtksArbiterMode:  None
  EtksMicroSecondTimeout:  100
  CheckJtagId:  y
  ConnOption:  Default
  UseExtReset:  y
  SetDebugEnableAb1DisablePin:  n
  OpenDrainReset:  n
  ResetWaitTime:  50
  HaltAfterReset:  y
  ChangeJtagClk:  4294967295
  ExecInitCmds:  y
  InvalidateCache:  y
  ChangeMsr:  n
  ChangeMsrValue:  0x00000000
  ResetPulseLen:  5
  InitScript Script:
    // disable watchdog
    SET 0xF4058010 0x0000C520
    SET 0xF4058010 0x0000D928
    SET 0xF4058000 0xFF00000A
    
    // cache invalidate
    SETSPR 0x3F2 0x00000003 0x00000003
    SETSPR 0x3F3 0x00000003 0x00000003
    SETSPR 0x3F2 0x00000000 0x00000003
    SETSPR 0x3F3 0x00000000 0x00000003
    
    // setup IVOPR
    // points to internal flash at 0x01000000
    SETSPR 0x3F 0x01000000 0xFFFFFFFF
    
    // setup SSCM error cfg for debug
    //SET16 SSCM_ERROR 0x3
    
    // reset CGM_AC0_SC to reset value because ESR0/PORST do not do so
    // CGM_AC0_SC must provide a valid clock in order to allow GTM debugging
    SET CGM_AC0_SC 0x00000000
    //SET CGM_AC0_SC 0x00000000
    
    // reset clock selector because it is not reset by hardware
    SET CGM_AC12_SC 0x00000000
    
    // disable reset escalation
    SET8 RGM_FRET 0x00
    SET8 RGM_DRET 0x00
    
  ExecOnConnectCmds:  n
  OnConnectScript Script:
    
  SimioAddr:  g_JtagSimioAccess
  FreezeTimers:  y
  AllowMmuSetup:  n
  ExecOnStartCmds:  n
  OnStartScript Script:
    
  ExecOnHaltCmds:  n
  ExecOnHaltCmdsWhileHalted:  n
  OnHaltScript Script:
    
  EnableProgramTimeMeasurement:  y
  TimerForPTM:  Default
  DefUserStreamChannel:  0
  DontUseCachedRegisters:  n
  AllowBreakOnUpdateBreakpoints:  n
  ClearDebugStatusOnHalt:  y
  UseRestartWhileRunningHandling:  n
  DoNotEnableSwBrk:  n
  TargetAppHandshakeMode:  None
  TargetAppHandshakeTimeout:  100
  TargetAppHandshakeParameter0:  0x00000000
  TargetAppHandshakeParameter1:  0x00000000
  TargetAppHandshakeParameter2:  0x00000000
  TargetAppHandshakeParameter3:  0x00000000
  UseNexus:  y
  DoSramInit:  y
  ForceCacheFlush:  n
  IgnoreLockedLines:  n
  HandleWdtBug:  n
  ForceEndOfReset:  n
  UseHwResetMode:  n
  HwResetMode:  Simulate
  WaitForEndOfBootCode:  n
  HandleNexusAccessBug:  y
  UseMasterNexusIfResetState:  y
  UseLocalAddressTranslation:  y
  Use64BitNexus:  n
  InitSramOnlyWhenNotInitialized:  n
  AllowHarrForUpdateDebugRegs:  n
  HaltOnDnh:  y
  AlwaysHaltOnDni:  y
  EnableLowPowerDebugHandshake:  n
  EnableLockstepDebug:  y
  AddBranchBeforeGo:  n
  InvalidTlbOnReset:  n
  DoNotEnableTrapSwBrp:  n
  AllowResetOnCheck:  n
  BootPasswd0:  0xFEEDFACE
  BootPasswd1:  0xCAFEBEEF
  BootPasswd2:  0xFEEDFACE
  BootPasswd3:  0xCAFEBEEF
  BootPasswd4:  0xFEEDFACE
  BootPasswd5:  0xCAFEBEEF
  BootPasswd6:  0xFEEDFACE
  BootPasswd7:  0xCAFEBEEF
  PasswordFile:  
  UsePasswordForUnlockDevice:  y
  DisableE2EECC:  n
  UseCore0ForNexusMemoryAccessWhileRunning:  n
  ForceDniForDebugger:  n
  HandleOvRamInitViaNarWorkaround:  n
  ApplySPC58NE84XoscWorkaround:  y
  ApplyEigerEdJtagWorkaround:  n
  IsUsedByTester:  n
  Mpc57xxClearPeripheralDebugAtNextCheckUserAppWhenRunning:  n
  SlaveHasHalted:  n

JTAG target infos:
  JTAG-ID:           0x00000000
  UsedJtagClk:       3000 kHz
  ExtVoltage:        0.0 V
  IntVoltageUsed:    n

Target infos:
  CoreName:  Core2
  FullCoreName:  Controller0.Core2
  ExtClock:  40000000
  IntClock:  16000000
  SysClock:  4000000
  StmClock:  8000000
  AccessToken:  0x1E6E
  HasNexus:  n
  BigEndian:  n
  CanSimio:  n
  CanPhysicalAccess:  n
  HasSpe:  n
  NumOfSimioChannels:  0
  JtagId:  0x00000000
  IsEarlyStep:  n
  IsMaster:  y
  MasterCoreName:  
  IsMasterEnabled:  y
  IsSlave:  n
  BuddyDeviceDetected:  n
  EtkConnected:  n
  Data TLB size on target:  0x00000010
  Instruction TLB size on target:  0x00000010
  Shared TLB size on target:  0x00000010
  Number of data TLB entries:  0x0000000C
  Number of instruction TLB entries:  0x00000006
  Number of shared TLB entries:  0x00000006
  Extended E200 MMU:  n
  E200 MPU:  y
  Data cache size:  0x00001000
  Data cache ways:  0x00000002
  Data cache sets:  0x00000040
  Data cache entry size:  0x00000024
  Instruction cache size:  0x00002000
  Instruction cache ways:  0x00000002
  Instruction cache sets:  0x00000080
  Instruction cache entry size:  0x00000024
  Unified Cache:  n
  MCM base address:  0xF4028000
  SIU base address:  0xF7FC0000
  Nexus On Slave:  n
  Core Number:  2
  Has Wdt bug:  n
  Length of IR register:  0x00000006
  Has Data Value comparators:  y
  Reset Mode:  0x00000008
  STM timer base address:  0xF4070000
  MC_ME base address:  0xF7FB8000
  Core in Lockstep mode:  n
  Core in DPM mode:  n
  Core is HSM:  n
  Core is Master of HSM:  n
  Name of other Master Core:  
  HsmBootEnabled:  n
  Target has Nexus access bug:  n
  Target has unlock JTAG capability:  y
  Unlock JTAG password len:  0x00000100
  Has JTAG unlock enable bit:  n
  ExecuteOpcodeAddr:  0x01000000
  IMEMBaseAddr:  0xFFFFFFFF
  IMEMSize:  0x00000000
  DMEMBaseAddr:  0x52800000
  DMEMSize:  0x00010000
  BootCodeStartAddr:  0x00404100
  HasCJtag:  y
  HasLfast:  n
  HasJtagOverCan:  y
  HasNpcLowPowerHandshake:  n
  HasLockstepDebug:  n
  TargetIsForSpc5UdeStk:  y
  PllCalc:  UDE.SPC58ECPllCalc
  JtagIdWhiteList:  0x00142041,0x0FFFFFFF
  JtagIdBlackList:  
  DciPinControl:  
  DciControl:  
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000001
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000000
  JtagChainType:  UNKNOWN
  JtagChainNumber:  0x00000000
  PowerPc system type:  MCKINLEY
  PowerPc synchonized GO type:  MPC5XXXPAR
  InactiveAfterReset:  n
  NumOfActiveCores:  1

Communication device:
  Type/Firmware:  FtdiCommDev V2.1.1
  Serial Number:  39511

Communication protocol handler:
  LastCmd:      0x0410
  LastResult:   0xC000
  ExpBytes:     580 (0x0244)
  RetBytes:     580 (0x0244)
  LastTimeout:  120010

Protocol diagnostic output:
  LastJtagApiAddr:   0x00000000
  LastJtagApiSpr:    0x00000000
  LastJtagApiDcr:    0x00000000
  LastJtagApiError:  0x00000000
  LastJtagApiStatus: 0x00000000
  JtagApiErrorLine:  1721
  JtagApiAddInfo0:   0x00000000
  JtagApiAddInfo1:   0x00000000
  ProtErrorLine:     5218
  LowLevelDiag0:     DEADBEEF (3735928559)
  LowLevelDiag1:     DEADBEEF (3735928559)
  LowLevelDiag2:     DEADBEEF (3735928559)
  LowLevelDiag3:     DEADBEEF (3735928559)


----------------------------------------------------------

  Connection Failed Report from
  Basic UDE Target Interface, Version: 1.18.9
  created: 04/11/22, 14:51:21

----------------------------------------------------------

Windows version:
  Win8 ()
  Admin: yes

UDE version:
  Release:  5.02.04
  Build:    8310
  Path:     C:\Program Files (x86)\pls\UDE Starterkit 5.2

Target configuration file:
  C:\Users\artur\Desktop\central_controller\SPC58_Workspace\ADC_Test\UDE\stm_spc58ec80_core2_core0_debug_jtag.cfg

Settings:
  PortType:  Default
  CommDevSel:  
  JtagViaPod:  n
  TargetPort:  Default
  JtagTapNumber:  0
  JtagNumOfTaps:  1
  JtagNumIrBefore:  0
  JtagNumIrAfter:  0
  UseExtendedCanId:  n
  JtagOverCanIdA:  0x00000001
  JtagOverCanIdB:  0x00000002
  JtagOverCanIdC:  0x00000003
  JtagOverCanIdD:  0x00000004
  JtagOverCanIdE:  0x00000005
  JtagmTckSel:  3
  JtagmInterFrameTimer:  0
  MaxJtagClk:  5000
  AdaptiveJtagPhaseShift:  y
  JtagMuxPort:  0
  JtagMuxWaitTime:  0
  JtagIoType:  Jtag
  EtksArbiterMode:  None
  EtksMicroSecondTimeout:  100
  CheckJtagId:  y
  ConnOption:  Default
  UseExtReset:  y
  SetDebugEnableAb1DisablePin:  n
  OpenDrainReset:  n
  ResetWaitTime:  50
  HaltAfterReset:  y
  ChangeJtagClk:  4294967295
  ExecInitCmds:  y
  InvalidateCache:  y
  ChangeMsr:  n
  ChangeMsrValue:  0x00000000
  ResetPulseLen:  5
  InitScript Script:
    // disable watchdog
    SET 0xF4058010 0x0000C520
    SET 0xF4058010 0x0000D928
    SET 0xF4058000 0xFF00000A
    
    // cache invalidate
    SETSPR 0x3F2 0x00000003 0x00000003
    SETSPR 0x3F3 0x00000003 0x00000003
    SETSPR 0x3F2 0x00000000 0x00000003
    SETSPR 0x3F3 0x00000000 0x00000003
    
    // setup IVOPR
    // points to internal flash at 0x01000000
    SETSPR 0x3F 0x01000000 0xFFFFFFFF
    
    // setup SSCM error cfg for debug
    //SET16 SSCM_ERROR 0x3
    
    // reset CGM_AC0_SC to reset value because ESR0/PORST do not do so
    // CGM_AC0_SC must provide a valid clock in order to allow GTM debugging
    SET CGM_AC0_SC 0x00000000
    //SET CGM_AC0_SC 0x00000000
    
    // reset clock selector because it is not reset by hardware
    SET CGM_AC12_SC 0x00000000
    
    // disable reset escalation
    SET8 RGM_FRET 0x00
    SET8 RGM_DRET 0x00
    
  ExecOnConnectCmds:  n
  OnConnectScript Script:
    
  SimioAddr:  g_JtagSimioAccess
  FreezeTimers:  y
  AllowMmuSetup:  n
  ExecOnStartCmds:  n
  OnStartScript Script:
    
  ExecOnHaltCmds:  n
  ExecOnHaltCmdsWhileHalted:  n
  OnHaltScript Script:
    
  EnableProgramTimeMeasurement:  y
  TimerForPTM:  Default
  DefUserStreamChannel:  0
  DontUseCachedRegisters:  n
  AllowBreakOnUpdateBreakpoints:  n
  ClearDebugStatusOnHalt:  y
  UseRestartWhileRunningHandling:  n
  DoNotEnableSwBrk:  n
  TargetAppHandshakeMode:  None
  TargetAppHandshakeTimeout:  100
  TargetAppHandshakeParameter0:  0x00000000
  TargetAppHandshakeParameter1:  0x00000000
  TargetAppHandshakeParameter2:  0x00000000
  TargetAppHandshakeParameter3:  0x00000000
  UseNexus:  y
  DoSramInit:  y
  ForceCacheFlush:  n
  IgnoreLockedLines:  n
  HandleWdtBug:  n
  ForceEndOfReset:  n
  UseHwResetMode:  n
  HwResetMode:  Simulate
  WaitForEndOfBootCode:  n
  HandleNexusAccessBug:  y
  UseMasterNexusIfResetState:  y
  UseLocalAddressTranslation:  y
  Use64BitNexus:  n
  InitSramOnlyWhenNotInitialized:  n
  AllowHarrForUpdateDebugRegs:  n
  HaltOnDnh:  y
  AlwaysHaltOnDni:  y
  EnableLowPowerDebugHandshake:  n
  EnableLockstepDebug:  y
  AddBranchBeforeGo:  n
  InvalidTlbOnReset:  n
  DoNotEnableTrapSwBrp:  n
  AllowResetOnCheck:  n
  BootPasswd0:  0xFEEDFACE
  BootPasswd1:  0xCAFEBEEF
  BootPasswd2:  0xFEEDFACE
  BootPasswd3:  0xCAFEBEEF
  BootPasswd4:  0xFEEDFACE
  BootPasswd5:  0xCAFEBEEF
  BootPasswd6:  0xFEEDFACE
  BootPasswd7:  0xCAFEBEEF
  PasswordFile:  
  UsePasswordForUnlockDevice:  y
  DisableE2EECC:  n
  UseCore0ForNexusMemoryAccessWhileRunning:  n
  ForceDniForDebugger:  n
  HandleOvRamInitViaNarWorkaround:  n
  ApplySPC58NE84XoscWorkaround:  y
  ApplyEigerEdJtagWorkaround:  n
  IsUsedByTester:  n
  Mpc57xxClearPeripheralDebugAtNextCheckUserAppWhenRunning:  n
  SlaveHasHalted:  n

JTAG target infos:
  JTAG-ID:           0x10142041
  UsedJtagClk:       3000 kHz
  ExtVoltage:        0.0 V
  IntVoltageUsed:    n

Target infos:
  CoreName:  Core2
  FullCoreName:  Controller0.Core2
  ExtClock:  40000000
  IntClock:  16000000
  SysClock:  4000000
  StmClock:  8000000
  AccessToken:  0x2B8D
  HasNexus:  y
  BigEndian:  y
  CanSimio:  y
  CanPhysicalAccess:  y
  HasSpe:  n
  NumOfSimioChannels:  1
  JtagId:  0x10142041
  IsEarlyStep:  n
  IsMaster:  y
  MasterCoreName:  
  IsMasterEnabled:  y
  IsSlave:  n
  BuddyDeviceDetected:  n
  EtkConnected:  n
  Data TLB size on target:  0x00000010
  Instruction TLB size on target:  0x00000010
  Shared TLB size on target:  0x00000010
  Number of data TLB entries:  0x0000000C
  Number of instruction TLB entries:  0x00000006
  Number of shared TLB entries:  0x00000006
  Extended E200 MMU:  n
  E200 MPU:  y
  Data cache size:  0x00001000
  Data cache ways:  0x00000002
  Data cache sets:  0x00000040
  Data cache entry size:  0x00000024
  Instruction cache size:  0x00002000
  Instruction cache ways:  0x00000002
  Instruction cache sets:  0x00000080
  Instruction cache entry size:  0x00000024
  Unified Cache:  n
  MCM base address:  0xF4028000
  SIU base address:  0xF7FC0000
  Nexus On Slave:  n
  Core Number:  2
  Has Wdt bug:  n
  Length of IR register:  0x00000006
  Has Data Value comparators:  y
  Reset Mode:  0x00000008
  STM timer base address:  0xF4070000
  MC_ME base address:  0xF7FB8000
  Core in Lockstep mode:  n
  Core in DPM mode:  n
  Core is HSM:  n
  Core is Master of HSM:  n
  Name of other Master Core:  
  HsmBootEnabled:  n
  Target has Nexus access bug:  n
  Target has unlock JTAG capability:  y
  Unlock JTAG password len:  0x00000100
  Has JTAG unlock enable bit:  n
  ExecuteOpcodeAddr:  0x01000000
  IMEMBaseAddr:  0xFFFFFFFF
  IMEMSize:  0x00000000
  DMEMBaseAddr:  0x52800000
  DMEMSize:  0x00010000
  BootCodeStartAddr:  0x00404100
  HasCJtag:  y
  HasLfast:  n
  HasJtagOverCan:  y
  HasNpcLowPowerHandshake:  n
  HasLockstepDebug:  n
  TargetIsForSpc5UdeStk:  y
  PllCalc:  UDE.SPC58ECPllCalc
  JtagIdWhiteList:  0x00142041,0x0FFFFFFF
  JtagIdBlackList:  
  DciPinControl:  
  DciControl:  
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000001
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000000
  JtagChainType:  UNKNOWN
  JtagChainNumber:  0x00000000
  PowerPc system type:  MCKINLEY
  PowerPc synchonized GO type:  MPC5XXXPAR
  InactiveAfterReset:  y
  NumOfActiveCores:  1

Communication device:
  Type/Firmware:  FtdiCommDev V2.1.1
  Serial Number:  39511

Communication protocol handler:
  LastCmd:      0x0412
  LastResult:   0xC01C
  ExpBytes:     560 (0x0230)
  RetBytes:     560 (0x0230)
  LastTimeout:  10000

Protocol diagnostic output:
  LastJtagApiAddr:   0x400A8400
  LastJtagApiSpr:    0x00000008
  LastJtagApiDcr:    0x00000000
  LastJtagApiError:  0x00000000
  LastJtagApiStatus: 0x00000201
  JtagApiErrorLine:  5663
  JtagApiAddInfo0:   0x00000000
  JtagApiAddInfo1:   0x00000000
  ProtErrorLine:     1471
  LowLevelDiag0:     00000000 (0)
  LowLevelDiag1:     00000000 (0)
  LowLevelDiag2:     00000000 (0)
  LowLevelDiag3:     00000000 (0)


