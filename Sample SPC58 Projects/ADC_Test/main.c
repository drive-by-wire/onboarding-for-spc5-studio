/****************************************************************************
*
* Copyright © 2015-2021 STMicroelectronics - All Rights Reserved
*
* This software is licensed under SLA0098 terms that can be found in the
* DM00779817_1_0.pdf file in the licenses directory of this software product.
* 
* THIS SOFTWARE IS DISTRIBUTED "AS IS," AND ALL WARRANTIES ARE DISCLAIMED, 
* INCLUDING MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*
*****************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include <stdio.h>
#include <stdlib.h>
#include "components.h"
#include "saradc_lld_cfg.h"
#include "pwm_lld_cfg.h"
/*
 * Application entry point.
 */

#define ADC_CHAN 63U
#define TS 50 // Time step in milliseconds
#define TICKS_PER_ROT 819
#define TICK_PERIOD 100 //10 kHz
#define DELAY 0
#define TRIGGER 0
#define CH0 0
#define CH1 1

static uint16_t value;
static uint8_t isDataReady;

void myConf_adc_isr(SARADCDriver *saradcp){
	value = saradc_lld_readchannel(saradcp, ADC_CHAN);
	isDataReady = 1;
}

int main(void) {
	uint16_t cmdPos = 3000; // Commanded position CHANGE THIS VALUE

	uint16_t curPos = 0; // Current position
//	uint32_t prevPos = 0; // Previous position
	int16_t err = 0; // Error
	float u_p = 0; // Proportional controller
	float u_i = 0; // Integral controller
	float a0 = 0; // Current accumulator
	float a1 = 0; // Previous accumulator
	float u0 = 0; // Current control effort
	uint8_t dutyCycle = 0; // PWM dutyCycle
	value = 0;
	isDataReady = 0;

  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/
  componentsInit();

  /* Uncomment the below routine to Enable Interrupts. */
  irqIsrEnable();
  
  // Enable PWM Group
  pwm_lld_init();
  pwm_lld_start(&PWMD9, &pwm_config_h_bridge);

  // Enable SARADC
  saradc_lld_start(&SARADC12DSV, &saradc_config_myConf);
  saradc_lld_start_conversion(&SARADC12DSV);

  /* Application main loop.*/
  for ( ; ; ) {
	  while(osalThreadGetMilliseconds() % TS);
	  while(!isDataReady);
	  curPos = value;
	  err = cmdPos - curPos;
	  u_p = 1.8 * (float)err;
	  a0 = a1 + (float)err * 0.05;
	  u_i = (0.06 * a0);
	  u0 = u_p + u_i;
	  if(abs(u0) > TICKS_PER_ROT){
		  a0 = a1;
		  u_i = 0;
		  u0 = (u0 > 0)? TICKS_PER_ROT : -TICKS_PER_ROT;
	  }

	  dutyCycle = (uint8_t) 100 * abs(u0) / TICKS_PER_ROT;

	  printf("ADC: %d, u0: %f\r\n", curPos, u0);

	  if(u0 > 0){
		  pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, 0, DELAY, TRIGGER);
		  pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, dutyCycle, DELAY, TRIGGER);
	  }
	  else{
		  pwm_lld_enable_channel(&PWMD9, CH0, TICK_PERIOD, 0, DELAY, TRIGGER);
		  pwm_lld_enable_channel(&PWMD9, CH1, TICK_PERIOD, dutyCycle, DELAY, TRIGGER);
	  }


	  isDataReady = 0;
	  a1 = a0;
	  osalThreadDelayMilliseconds(1);

  }
}
