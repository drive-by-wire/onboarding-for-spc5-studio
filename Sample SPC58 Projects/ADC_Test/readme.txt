1. Enable SARADC under "Low Level Drivers Components RLA"
2. Under SARADC Settings, check "SARADC 12 Bit Supervisor"
3. Create a new SARADC Config
4. Set Conversion Mode to Scan
5. Add a conversion callback routine (Note: When calling the ISR from main, the name is <ADC configuration name>_<ISR name>)
6. Go to Pinmap Editor->"ADCSAR [ADC_SAR_12b_3]" and choose a channel (Note: 61 and 64 are the on board potentiometers)
7. Go back to the SARADC Config, and add a channel configuration
8. Set the channel number to match the ADC channel number on the pinmap