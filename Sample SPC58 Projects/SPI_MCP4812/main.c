/****************************************************************************
*
* Copyright © 2015-2021 STMicroelectronics - All Rights Reserved
*
* This software is licensed under SLA0098 terms that can be found in the
* DM00779817_1_0.pdf file in the licenses directory of this software product.
* 
* THIS SOFTWARE IS DISTRIBUTED "AS IS," AND ALL WARRANTIES ARE DISCLAIMED, 
* INCLUDING MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*
*****************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include "components.h"
#include "spi_lld_cfg.h"

/*
 * @function dac_message
 * @param *buf a pointer to a buffer array minsize 2
 * @param level	an integer value between 0 and 1023
 * @return None
 * @brief Generates a message for the MCP4812 to be
 *        passed through SPI.
 */
void dac_message(uint8_t *buf, uint16_t level);

#define SPI_LEN 2

static uint8_t spiBuf[SPI_LEN];
static uint8_t flag;

/*
 * Application entry point.
 */
int main(void) {
	flag = 0;
	for(int i=0; i <SPI_LEN; i++)
		spiBuf[i] = 0;
  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/
  componentsInit();

  /* Uncomment the below routine to Enable Interrupts. */
  irqIsrEnable();
  
  spi_lld_start(&SPID1, &spi_config_mcp4812);   /* Setup parameters.       */

  /* Application main loop.*/
  for ( ; ; ) {
	  if(flag){
		dac_message(spiBuf, 512);
		spi_lld_send(&SPID1, 2, spiBuf);
		flag = 0;
	  }
	  else{
		dac_message(spiBuf, 0);
		spi_lld_send(&SPID1, 2, spiBuf);
		flag = 1;
	  }

	  osalThreadDelayMilliseconds(1);
  }
}

void dac_message(uint8_t *buf, uint16_t level){
	level = level & 0x3FF;
	buf[0] = 0x10 + (level >> 6);
	buf[1] = (level << 2) & 0xFF;
}


