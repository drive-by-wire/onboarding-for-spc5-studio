/*
 * main.h
 *
 *  Created on: Apr 13, 2022
 *      Author: artur
 */

#ifndef MAIN_H_
#define MAIN_H_

/*
*  Authors: Arturo Gamboa Gonzales, Sutter Lum, Sriram Venkataraman
*  Description: Header file for the HSL Drive-by-Wire Senior Design Project
*/

/* ============================================================================
 *                           DEFINES SECTION
 * ==========================================================================*/

#define RX_LEN 20

/* ============================================================================
 *                           ENUMS SECTION
 * ==========================================================================*/

typedef enum {
    START,      // Vehicle stays here until a switch is pressed (indicated by input_mode)
    SETUP,      // Perform necessary actions to put vehicle in OPERATION state
    OPERATION,  // The vehicle will remain in this state under nominal conditions
    ERROR       // The system will remain idle in this state until the reset is pressed
} CarState;

typedef enum {
    SYSTEMOFF, JOYSTICK, LAPTOP
} InputMode;

typedef enum {
    True, False, SUCCESS, FAILURE
} Bool;

/* ============================================================================
 *                        CLASS DEFINITION SECTION
 * ==========================================================================*/

typedef struct {
        double steering_angle;
        double percent_brake;
        double percent_throttle;
        Bool horn;
        Bool reverse;
        Bool windshield_wiper;
        Bool E_STOP;
        Bool left_turn;
        Bool right_turn;

} CarValues;


void carValuesInit(CarValues* carValues) {
	carValues->steering_angle = 0;
	carValues->percent_brake = 0;
	carValues->percent_throttle = 0;
	carValues->horn = False;
	carValues->reverse = False;
	carValues->windshield_wiper = False;
	carValues->E_STOP = False;
	carValues->left_turn = False;
	carValues->right_turn = False;
}

//void carValuesInit(CarValues carValues) {
//	carValues.steering_angle = 0;
//	carValues.percent_brake = 0;
//	carValues.percent_throttle = 0;
//	carValues.horn = False;
//	carValues.reverse = False;
//	carValues.windshield_wiper = False;
//	carValues.E_STOP = False;
//	carValues.left_turn = False;
//	carValues.right_turn = False;
//}


#endif /* MAIN_H_ */

