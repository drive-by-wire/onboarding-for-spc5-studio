/****************************************************************************
 *
 * Authors: Arturo Gamboa Gonzales, Sutter Lum, Sriram Venkataraman
 *
 * Description: Source file for the HSL Drive-by-Wire Senior Design Project
 *
 *****************************************************************************/

/* ============================================================================
 *                           INCLUDE SECTION
 * ==========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include "components.h"
#include "pit_lld_cfg.h"
#include "main.h"
#include "LEDs.h"
//#include "FreeRTOS.h"
//#include "task.h"

/* ============================================================================
 *                       PRIVATE VARIABLES SECTION
 * ==========================================================================*/

static InputMode input_mode = SYSTEMOFF;
static Bool in_motion = False;
static Bool reset_was_pressed = False;
static Bool system_OFF = False;
static Bool error = False;
static CarState current_state = START;
static uint8_t rx_buf[RX_LEN];


/* ============================================================================
 *                       PRIVATE FUNCTIONS SECTION
 * ==========================================================================*/

/*
 * Description: Commands the accelerator using a DAC
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *   SUCCESS if DAC is connected, FAILURE if disconnnected
 */
Bool updateThrottle(CarValues* current, CarValues* prev) {
	// print('setting throttle')
	return SUCCESS;
}

/*
 * Description: Commands the brake actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  SUCCESS if actuator is connected, FAILURE if disconnnected
 */
Bool updateBraking(CarValues* current, CarValues* prev) {
	// print('setting brake')
	return SUCCESS;
}

/*
 * Description: Commands the steering actuator to the desired position
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 * Return:
 *   SUCCESS if actuator is connected, FAILURE if disconnnected
 */
Bool updateSteering(CarValues* current, CarValues* prev) {
	// print('setting steering')
	return SUCCESS;
}

/*
 * Description: Toggles the left turn signal
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void updateLeftTurnSignal(CarValues* current) {
	return;
}

/*
 * Description: Toggles the right turn signal
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void updateRightTurnSignal(CarValues* current) {
	return;
}
/*
 * Description: Computes the nth Fibonacci number
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void updateHorn(CarValues* current) {
	return;
}

/*
 * Description: Computes the nth Fibonacci number
 * Arguments:
 *   current - container of the new desired values
 *   prev - container of the previous desired values
 *
 * Return:
 *  NULL
 */
void updateWindShieldWipers(CarValues* current) {
	return;
}

int main(void) {

	/* Initialization of all the imported components in the order specified in
	 the application wizard. The function is generated automatically.*/
	componentsInit();

	/* Enable Interrupts */
	irqIsrEnable();



	// create the containers for values
	CarValues* newVals = (CarValues*) malloc(sizeof(CarValues));
	carValuesInit(newVals);

	CarValues* prevVals = (CarValues*) malloc(sizeof(CarValues));
	carValuesInit(prevVals);

	// joystick init (serial)
	sd_lld_start(&SD1, NULL);

	//ethernet init

	// initialize LEDs
	initLEDs();
	systemOffLED();

	// timer to read input buttons
	pit_lld_start(&PITD1, pit0_config);
	pit_lld_channel_start(&PITD1,PIT0_CHANNEL_CH1);

	while (1) {
		switch (current_state) {

		/**********************************************************************************
		* START State: Vehicle stays here until a switch is pressed (indicated by input_mode)
		***********************************************************************************/
		case (START):

			if (input_mode != SYSTEMOFF) {

				if (in_motion == True) {
					current_state = ERROR;
					systemOffLED();
				}

				else current_state = SETUP;

			}
			break;

		/**********************************************************************************
		* SETUP State: Perform necessary actions to put vehicle in OPERATION state
		***********************************************************************************/
		case (SETUP):

			error = False;

			// check power on HV step down circuit, if isolation fault error = 1
			// run brake actuator test, if no response error = 1
			// run steering actuator test, if no response error = 1

			if (error == False) {
				current_state = OPERATION;
				(input_mode == JOYSTICK) ? joystickLED() : laptopLED();
			} else {
				current_state = ERROR;
				systemOffLED();
			}
			break;

		/**********************************************************************************
		* OPERATION State: The vehicle will remain in this state under nominal conditions
		***********************************************************************************/
		case (OPERATION):

			// check if the laptop is connected, and parse the values
			if (input_mode == LAPTOP) {
//				  msg = eth_recv // read the response message
//                newVals, errorCode, checksumError = packet_to_value(msg);
//                if (checksumError == True) {
//                	current_state = CarState;
//                	break;
//                }
			}

			// check if the joystick is connected, and parse the values
			if (input_mode == JOYSTICK) {
				sd_lld_read(&SD1, rx_buf, 13);
//				parse_message(newVals, rx_buf);
//				clear_buffer(rx_buf, RX_LEN);
			}

			if ( (newVals->E_STOP) == True) {
				current_state = ERROR;
				systemOffLED();
				break;
			}

			if (updateThrottle(newVals, prevVals) == FAILURE) {
				current_state = ERROR;
				systemOffLED();
				break;
			}

			if (updateBraking(newVals, prevVals) == FAILURE) {
				current_state = ERROR;
				systemOffLED();
				break;
			}

			if (updateSteering(newVals, prevVals) == FAILURE) {
				current_state = ERROR;
				systemOffLED();
				break;
			}

			updateLeftTurnSignal(newVals);

			updateRightTurnSignal(newVals);

			updateHorn(newVals);

			updateWindShieldWipers(newVals);

			prevVals = newVals;

			break;

		/**********************************************************************************
		* ERROR State: The system will remain idle in this state until the reset is pressed
		***********************************************************************************/
		case (ERROR):
			if (reset_was_pressed == True) {
				current_state = START;
				input_mode = SYSTEMOFF;
				reset_was_pressed = False;
				carValuesInit(newVals);
				carValuesInit(prevVals);
			}
			break;

		/**********************************************************************************
		* Update the display with pertinent information
		***********************************************************************************/
		//figure out i2c coms for isr

		}

	}

}

/* ============================================================================
 *                           ISR FUNCTIONS
 * ==========================================================================*/

///*
// * Description: BSP Timer ISR Handler
// * Arguments:
// *   Void
// * Return:
// *   NULL
// */
//void cb_stm0_ch0(void) {
//	uint8_t message[] = "STM0 Channel 0 expired!\r\n";
//	while (sd_lld_write(&SD1, message,
//			(uint16_t) (sizeof(message) / sizeof(message[0])))
//			== SERIAL_MSG_WAIT) {
//	}
//	// POLL FOR INPUT MODE, SYSTEM OFF, JOYSTICK TRIGGERED, LAPTOP TRIGGERED
//}

void pit_isr(void){
	printf("%d\r\n", pal_lld_readpad(PORT_E, PIN_GPIO71));
}

