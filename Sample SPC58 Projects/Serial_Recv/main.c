/****************************************************************************
*
* Copyright © 2015-2021 STMicroelectronics - All Rights Reserved
*
* This software is licensed under SLA0098 terms that can be found in the
* DM00779817_1_0.pdf file in the licenses directory of this software product.
* 
* THIS SOFTWARE IS DISTRIBUTED "AS IS," AND ALL WARRANTIES ARE DISCLAIMED, 
* INCLUDING MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*
*****************************************************************************/

/* Inclusion of the main header files of all the imported components in the
   order specified in the application wizard. The file is generated
   automatically.*/
#include <stdio.h>
#include "components.h"
#include "serial_lld_cfg.h"

#define RX_LEN 32
/*
 * Application entry point.
 */

static uint8_t rxBuf[RX_LEN]; // Receive buffer
static uint8_t bytes; // Offset from rx pointer start
static uint8_t txFlag; // Ready to transmit. Set to false after serial send

int main(void) {
  /* Initialization of all the imported components in the order specified in
     the application wizard. The function is generated automatically.*/

  uint8_t message[] = "Hello World\r\n";
  bytes = 0;
  txFlag = TRUE;

  for(int i = 0; i< RX_LEN; i++)
	  rxBuf[i] = 0; // Clear buffer

  componentsInit();
  irqIsrEnable();

  printf("%s", message); // Sign of life

  sd_lld_start(&SD1, &serial_config_mySerialCfg); // Serial start

  /* Application main loop.*/
  for ( ; ; ) {
	  // The IO Buffer is a circular buffer, so we check when the read and write ptrs are different
	  while((int)SD1.rx_write_ptr != (int)SD1.rx_read_ptr){
		  bytes += sd_lld_read(&SD1, (rxBuf + bytes), 1);
	  }

	  if(bytes != 0){
		  printf("%s\r\n", rxBuf); // Print message
		  for(int i = 0; i < bytes; i++)
		  	  rxBuf[i] = 0; // Clear buffer
		  bytes = 0; // reset index
	  }
	  else
		  printf("wait\r\n"); // Proof code is non blocking
	  osalThreadDelayMilliseconds(250); // Just here to not overload the terminal
  }
}

void tx_isr(SerialDriver *sdp){
	(void)sdp;
	txFlag = TRUE;
}
